# Working with Backend

## How to build docker

### Build docker image

    docker-compose up --build -d

## How to run django app.

### Enter in docker web container with.

    docker exec -it wac_web_1 bash

### Run migrations

    python manage.py migrate

### Load fixtures

    python manage.py loaddata api_user/fixtures/fixture.json

    Emails:
        - admin@example.com
        - user0@example.com
        - user1@example.com
        - user2@example.com
        - user3@example.com

    Password:
        - 12345678abc

### Start app.

    python manage.py runserver 0:8000

## URLS

    API: http://localhost:8000/api/user/
    Login: http://localhost:8000/api/login/
    Token refresh: http://localhost:8000/api/token/refresh/