from rest_framework.permissions import BasePermission


class ItsMyProfilePermission(BasePermission):
    """
    Check if it is my profile.
    """

    def has_permission(self, request, view):
        return bool(
            request.user.pk == int(request.parser_context.get("kwargs").get("pk"))
        )
