from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from .models import User


class BaseUserSerializer(serializers.ModelSerializer):
    """
    Base serializer to create / update an user.
    """

    def validate_email(self, value):
        """
        Check if there is a deleted user with the same mail.
        :param value:
        :return:
        """
        if (self.instance and self.instance.email != value) or self.instance is None:
            if User.objects.filter(email=value).exists():
                raise serializers.ValidationError(
                    {"email": _("There's already a user with that email.")}
                )

        return value

    def validate_username(self, value):
        """
        Check if there is a deleted user with the same username.
        :param value:
        :return:
        """
        if (self.instance and self.instance.username != value) or self.instance is None:
            if User.objects.filter(username=value).exists():
                raise serializers.ValidationError(
                    {"username": _("There's already a user with that username.")}
                )

        return value

    def validate(self, data):
        """
        Check that start is before finish.
        """
        password2 = data.get("password2")
        if "password2" in data.keys():
            data.pop("password2")
        if data.get("password") != password2:
            message = _("Passwords must match.")
            raise serializers.ValidationError(
                {"password": message, "password2": message}
            )
        return data


class CreateUserSerializer(BaseUserSerializer):
    """
    Basic serializer to create an user.
    """

    password = serializers.CharField(style={"input_type": "password"}, write_only=True)
    password2 = serializers.CharField(style={"input_type": "password"}, write_only=True)

    def create(self, validated_data):
        """
        Override the method to activate user.
        :param validated_data:
        :return:
        """
        validated_data.update(
            {
                "is_active": True,
                "password": make_password(validated_data.get("password")),
            }
        )
        return super().create(validated_data)

    class Meta:
        model = User
        fields = (
            "email",
            "username",
            "first_name",
            "last_name",
            "avatar",
            "password",
            "password2",
        )


class UpdateUserSerializer(BaseUserSerializer):
    """
    Serializer to user data
    """

    password = serializers.CharField(
        style={"input_type": "password"}, write_only=True, allow_blank=True
    )
    password2 = serializers.CharField(
        style={"input_type": "password"}, write_only=True, allow_blank=True
    )

    def update(self, instance, validated_data):
        """
        Override the method to activate user.
        :param validated_data:
        :return:
        """
        validated_data.update(
            {
                "password": make_password(validated_data.get("password")),
            }
        )
        return super().update(instance, validated_data)

    class Meta:
        model = User
        fields = (
            "email",
            "username",
            "first_name",
            "last_name",
            "avatar",
            "password",
            "password2",
        )


class ListRetrieveUserSerializer(serializers.ModelSerializer):
    """
    Serializer to user data
    """

    class Meta:
        model = User
        fields = (
            "pk",
            "email",
            "username",
            "first_name",
            "last_name",
            "avatar",
        )
