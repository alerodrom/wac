from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import User
from .permissions import ItsMyProfilePermission
from .serializers import (
    CreateUserSerializer,
    ListRetrieveUserSerializer,
    UpdateUserSerializer,
)


class UserViewSet(ModelViewSet):
    """
    Class-based view for User Model.
    """

    model = User
    queryset = User.objects.all()
    http_method_names = ["get", "post", "patch", "put"]
    serializer_action_classes = {
        "list": ListRetrieveUserSerializer,
        "create": CreateUserSerializer,
        "retrieve": ListRetrieveUserSerializer,
        "partial_update": UpdateUserSerializer,
        "update": UpdateUserSerializer,
    }

    def get_serializer_class(self):
        """
        Override the method for modifying the logic.
        :return:
        """
        try:
            return self.serializer_action_classes.get(self.action)
        except (KeyError, AttributeError):
            return super(UserViewSet, self).get_serializer_class()

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        :return:
        """
        permission_classes = []
        if self.action is not "create":
            permission_classes = [IsAuthenticated]
            if self.action in ["partial_update", "update"]:
                permission_classes += [ItsMyProfilePermission]

        return (
            [permission() for permission in permission_classes]
            if permission_classes
            else []
        )


class UserLoginView(TokenObtainPairView):
    """
    Class to login an user's.
    """

    serializer_class = TokenObtainPairSerializer
