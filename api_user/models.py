from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


def upload_path(instance, file):
    """
    Function that returns the path where to save the profile image, also it
    removes the old images.
    :param instance:
    :return:
    """
    return instance.email


class User(AbstractUser):
    """
    User model.
    """

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    email = models.EmailField(_("Email"), unique=True)
    avatar = models.ImageField(
        _("Avatar"),
        max_length=1000,
        upload_to=upload_path,
        default="default/icon-default-user.png",
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
