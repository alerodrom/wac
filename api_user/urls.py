from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt import views as jwt_views

from . import views

app_name = "api_user"

router = DefaultRouter()


router.register(
    r"user",
    views.UserViewSet,
    basename="user",
)

urlpatterns = router.urls

urlpatterns += [
    path("login/", views.UserLoginView.as_view(), name="login"),
    path("token/refresh/", jwt_views.TokenRefreshView.as_view(), name="token_refresh"),
]
