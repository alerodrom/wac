from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api_user.models import User


class WacAPITestCase(APITestCase):
    """WAC API test case."""

    fixtures = ("fixture.json",)

    def setUp(self):
        """Override set up to get data"""

        # URLS
        self.url_login = reverse("user:login")
        self.url_api_user_list = reverse("user:user-list")
        # self.url_api_user_detail = reverse("user:user-detail")

        # Users
        self.user_0 = User.objects.get(email="user0@example.com")
        self.user_1 = User.objects.get(email="user1@example.com")
        self.user_2 = User.objects.get(email="user2@example.com")
        self.user_3 = User.objects.get(email="user3@example.com")

    def get_login_data(self, user):
        """
        Get login data dict.
        :param user:
        :return:
        """
        return {"email": user.email, "password": "12345678abc"}

    def get_login_data_fail(self):
        """
        Get login data dict.
        :param user:
        :return:
        """
        return {"email": "fail@example", "password": "12345678abc"}

    register_data = {
        "username": "wacuser",
        "first_name": "Wac",
        "last_name": "User",
        "email": "wacuser@example.com",
        "password": "12345678abc",
        "password2": "12345678abc",
    }

    register_data_fail = {
        "username": "wacuserfail",
        "first_name": "Wac",
        "last_name": "User",
        "email": "wacuserfail@example.com",
        "password": "12345678abc",
        "password2": "abc12345678",
    }

    updated_data = {
        "first_name": "WacEdit",
        "last_name": "UserEdit",
    }

    def test_login(self):
        """
        Ensure we can login a new account object.
        """
        # Login user 0
        response = self.client.post(
            self.url_login, self.get_login_data(self.user_0), format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_false(self):
        """
        Ensure we can not login a new account object.
        """
        # Login user 0
        response = self.client.post(
            self.url_login, self.get_login_data_fail(), format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_register_pass(self):
        """Test to register an user."""
        response = self.client.post(
            self.url_api_user_list, self.register_data, format="json"
        )
        user = User.objects.get(email=self.register_data.get("email"))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(user.email, self.register_data.get("email"))
        self.assertEqual(
            user.username,
            self.register_data.get("username"),
        )

    def test_register_fail(self):
        """Test to check password doesn't match."""
        response = self.client.post(
            self.url_api_user_list, self.register_data_fail, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_user(self):
        """Test to update user."""
        # Login user
        user = self.user_1
        response_login = self.client.post(
            self.url_login, self.get_login_data(user), format="json"
        )
        self.assertEqual(response_login.status_code, status.HTTP_200_OK)

        # Auth token
        self.client.credentials(
            HTTP_AUTHORIZATION="Bearer {}".format(response_login.data.get("access"))
        )
        response = self.client.patch(
            reverse("user:user-detail", kwargs={"pk": user.pk}),
            self.updated_data,
            format="json",
        )

        # Refresh user after update
        user.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_CREATED)
        self.assertEqual(user.first_name, self.updated_data.get("first_name"))
        self.assertEqual(
            user.last_name,
            self.updated_data.get("last_name"),
        )

    def test_register_fail(self):
        """Test to not update user."""
        response = self.client.post(
            self.url_api_user_list, self.register_data_fail, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
