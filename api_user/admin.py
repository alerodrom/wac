from django.contrib import admin

from .models import User


class UserAdmin(admin.ModelAdmin):
    """
    class UserAdmin
    """

    list_display = (
        "username",
        "email",
        "is_staff",
        "is_active",
        "date_joined",
    )


admin.site.register(User, UserAdmin)
